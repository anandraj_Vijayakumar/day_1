package triangle;

public class InvertedTriangle {
	public static void main(String[] args) {
		System.out.println("--------------");
        for(int i = 4; i >= 1; --i) {
            for(int j = 1; j <= 4 - i; ++j) {
                System.out.print("  ");
            }
            for(int j=i; j <= 2 * i - 1; ++j) {
                System.out.print("* ");
            }
            for(int j = 0; j < i - 1; ++j) {
                System.out.print("* ");
            }
            System.out.println();
        }
    }
}
